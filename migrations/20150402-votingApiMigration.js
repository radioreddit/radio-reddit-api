module.exports = {
  up: function(migration, DataTypes, done) {
    migration.addColumn(
    'Tracks',
    'redditPostFullname',
    DataTypes.STRING
  );
    done() // sets the migration as finished
  },
 
  down: function(migration, DataTypes, done) {
    migration.removeColumn('Tracks', 'redditPostFullname');
    done() // sets the migration as finished
  }
}