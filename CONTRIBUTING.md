
#radio reddit
##Contributor License Agreement
Thank you for your interest radio reddit! The form of license below is a document that clarifies the terms under which you, the person listed below, may contribute software, bug fixes, configuration changes, documentation, etc… to the project. We’re thrilled you want to participate, and we want you to understand what will be done with your contributions. This license is for your protection as well as the protection of radio reddit and its licensees; it does not change your rights to use your own contributions for any other purpose.  Please complete the following information about you and the contributions and sign electronically by filling out the form below.

If you have questions about these terms, please contact us at admin@radioreddit.com.

###Corporate Contributions:
If you are employed as a software engineer, or if your employer is in the business of developing software, or otherwise may claim rights in the contributions, please provide information about your employer's policy on contributing to open source projects, including the name of the supervisor to contact in connection with such contributions.  

You and radio reddit agree:

1.	You grant us the ability to use the contributions in any way. You hereby grant to radio reddit, a non-exclusive, irrevocable, worldwide, royalty-free, sublicenseable, transferable license under all of your relevant intellectual property rights (including copyright, patent, and any other rights), to use, copy, prepare derivative works of, distribute and publicly perform and display the contributions on any licensing terms, including without limitation: (a) open source licenses like the GNU General Public License (GPL), the MIT License (MIT), the GNU Lesser General Public License (LGPL), the Common Public License, or the Berkeley Software Distribution license (BSD); and (b) binary, proprietary, or commercial licenses. Except for the licenses granted herein, you reserve all right, title, and interest in and to the contribution.

2.	You are able to grant us these rights. You represent that you are legally entitled to grant the above license. If your employer has rights to intellectual property that you create, you represent that you have received permission to make the contributions on behalf of that employer, or that your employer has waived such rights for the contributions.

3.	The contributions are your original work. You represent that the contributions are your original works of authorship, and to your knowledge, no other person claims, or has the right to claim, any right in any invention or patent related to the contributions. You also represent that you are not legally obligated, whether by entering into an agreement or otherwise, in any way that conflicts with the terms of this license. For example, if you have signed an agreement requiring you to assign the intellectual property rights in the contributions to an employer or customer, that would conflict with the terms of this license.

4.	We determine the code that is in our project. You understand that the decision to include the contribution in any project or source repository is entirely that of radio reddit, and this agreement does not guarantee that the contributions will be included in any product.

5.	No Implied Warranties. Radio reddit acknowledges that, except as explicitly described in this Agreement, the contribution is provided on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING, WITHOUT LIMITATION, ANY WARRANTIES OR CONDITIONS OF TITLE, NON-INFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE.

TL;DR: Whatever you give us, we are allowed to use on radio reddit.  You also affirm that you’re allowed to give us work, IE you weren’t creating this software/image/description/etc… on your boss’ dime.  Also that you also created this work, you didn’t steal it. We don’t have to use any code unless we want to, and we understand that your code is provided ‘as is’.
You grant us the rights necessary to redistribute your work as part of our open source project. 
#Sign Electronically




