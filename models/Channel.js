module.exports = function (sequelize, DataTypes) {
  var Channel = sequelize.define('Channel', {
    name: DataTypes.STRING,
    description: DataTypes.TEXT
  }, {
    classMethods: {
      associate: function (models) {
        Channel.hasMany(models.Stream);
        Channel.hasMany(models.ChannelMetadata);
        Channel.belongsToMany(models.Genre, {through: 'ChannelGenres'});
      }
    }
  });
  
  return Channel;
};