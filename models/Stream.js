module.exports = function (sequelize, DataTypes) {
  var Stream = sequelize.define('Stream', {
    url: {
      type: DataTypes.STRING,
      validate: {
        isUrl: true
      }
    },
    contentType: {
      type: DataTypes.STRING,
      validate: {
        contains: '/'
      }
    },
    averageBitrate: DataTypes.INTEGER
  }, {
    classMethods: {
      associate: function (models) {
        Stream.belongsTo(models.Channel);
      }
    }
  });
  
  return Stream;
};