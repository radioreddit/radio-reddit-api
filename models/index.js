var fs = require('fs');
var path = require('path');
var _ = require('underscore');

module.exports = function (sequelize) {
  // Load everything in the models directory
  var models = {};
  var files = fs.readdirSync(__dirname);
  _.each(files, function (file) {
    if (
      path.extname(file) !== '.js' ||
      path.join(__dirname, file) === __filename
    ) {
      return;
    }
    
    var model = sequelize.import(path.join(__dirname, file))
    models[model.name] = model;
    
  });
  
  // Set up associations
  Object.keys(models).forEach(function (modelName) {
    if ('associate' in models[modelName]) {
      models[modelName].associate(models);
    }
  });
  
  return models;
}
