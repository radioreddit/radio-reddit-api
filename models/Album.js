var s3Util = require(__dirname + '/../lib/s3Util.js');

module.exports = function (sequelize, DataTypes) {
  var Album = sequelize.define('Album', {
    title: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    coverImage: DataTypes.STRING,
    isPodcast: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    description: DataTypes.TEXT,
    comments: DataTypes.TEXT // Recording lineage notes, credits, copyright, etc.
  }, {
    classMethods: {
      associate: function (models) {
        Album.belongsTo(models.Artist);
        Album.hasMany(models.Track);
        Album.belongsToMany(models.Genre, {through: 'AlbumsGenres'});
        Album.belongsTo(models.Feed);
      }
    },
    instanceMethods: {
      isWritableByUser: function (user) {
        return this.getArtist().then(function (artist) {
          return artist.isWritableByUser(user);
        });
        
      }
    },
    getterMethods: {
      art: function () {
        return s3Util.getAlbumArt(this);
      }
    },
    paranoid: true
  });
  
  return Album;
}