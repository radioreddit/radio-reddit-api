module.exports = function (sequelize, DataTypes) {
  var Feed = sequelize.define('Feed', {
    url: {
      type: DataTypes.STRING(2048),
      validate: {
        isUrl: true
      }
    }
  }, {
    classMethods: {
      associate: function (models) {
        Feed.hasOne(models.Album);
      }
    },
    paranoid: true
  });
  
  return Feed;
}