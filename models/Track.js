var s3Util = require(__dirname + '/../lib/s3Util.js');

module.exports = function (sequelize, DataTypes) {
  var Track = sequelize.define('Track', {
    title: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    description: DataTypes.TEXT,
    comments: DataTypes.TEXT, // Recording lineage notes, credits, copyright, etc.
    lyrics: DataTypes.TEXT,
    state: {
      type: DataTypes.ENUM(
        'Created',
        'Uploaded',
        'Approved',
        'Rejected',
        'DMCATakedown'
      )
    },
    allowDownload: DataTypes.BOOLEAN,
    allowRelease: DataTypes.BOOLEAN,
    
    // Old ID from Drupal site
    legacyId: DataTypes.INTEGER,
    legacyPath: DataTypes.STRING,
    
    recordingDate: DataTypes.DATE, // Recorded date, released date, podcast date
    
    albumSequence: DataTypes.INTEGER, // Track # on album, podcast sequence number
    redditPostFullname: DataTypes.TEXT
  }, {
    classMethods: {
      associate: function (models) {
        Track.belongsTo(models.Artist);
        Track.belongsTo(models.Album);
        Track.belongsTo(models.User);
        Track.hasMany(models.Vote);
        Track.belongsToMany(models.Genre, {through: 'TracksGenres'});
        Track.belongsToMany(models.User, {through: 'UsersFavorites'});
        Track.belongsToMany(models.Playlist, {through: models.PlaylistsTracks});
      }
    },
    
    instanceMethods: {
      isWritableByUser: function (user) {
        return this.getAlbum().then(function (album) {
          return album.isWritableByUser(user);
        });
      }
    },
    
    getterMethods: {
      media: function () {
        return s3Util.getTrackMedia(this);
      }
    },
    paranoid: true
  });
  
  return Track;
}