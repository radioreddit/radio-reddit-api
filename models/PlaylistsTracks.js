module.exports = function (sequelize, DataTypes) {
  var PlaylistsTracks = sequelize.define('PlaylistsTracks', {
    position: DataTypes.INTEGER
  }, {
    paranoid: true
  });
  
  return PlaylistsTracks;
}