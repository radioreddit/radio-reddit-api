module.exports = function (sequelize, DataTypes) {
  var Artist = sequelize.define('Artist', {
    name: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: true
      },
      allowNull: false
    },
    description: {
      type: DataTypes.TEXT
    },
    homeCity: {
      type: DataTypes.STRING
    },
    homeStateProvince: {
      type: DataTypes.STRING
    },
    homeCountry: {
      type: DataTypes.CHAR(2)
    },
    websiteUrl: {
      type: DataTypes.STRING(2048),
      validate: {
        isUrl: true
      }
    },
    googleAnalyticsPropertyId: {
      type: DataTypes.STRING,
      validate: {
        is: /^UA\-[0-9]{1,12}\-[0-9]{1,3}$/
      }
    }
  }, {
    classMethods: {
      associate: function (models) {
        Artist.belongsToMany(models.Genre, {through: 'ArtistsGenres'});
        Artist.belongsToMany(models.User, {through: 'UsersArtists'});
        Artist.hasMany(models.Album);
        Artist.hasMany(models.Track);
        
      }
    },
    instanceMethods: {
      isWritableByUser: function (user) {
        return this.hasUser(user);
      }
    },
    paranoid: true
  });
  
  return Artist;
};