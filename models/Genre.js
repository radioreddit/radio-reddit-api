module.exports = function (sequelize, DataTypes) {
  var Genre = sequelize.define('Genre', {
    name: {
      type: DataTypes.STRING,
      allowNull: false
    }
  }, {
    classMethods: {
      associate: function (models) {
        Genre.belongsTo(models.Genre, {as: 'parentGenre'});
        Genre.belongsToMany(models.Artist, {through: 'ArtistsGenres'});
        Genre.belongsToMany(models.Track, {through: 'TracksGenres'});
        Genre.belongsToMany(models.Album, {through: 'AlbumsGenres'});
        Genre.belongsToMany(models.Channel, {through: 'ChannelGenres'});
      }
    },
    timestamps: false
  });
  
  return Genre;
}