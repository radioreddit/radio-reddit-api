module.exports = function (sequelize, DataTypes) {
  var ChannelMetadata = sequelize.define('ChannelMetadata', {
    artist: {
      type: DataTypes.STRING
    },
    title: {
      type: DataTypes.STRING(1024)
    },
    filename: {
      type: DataTypes.STRING(1024)
    },
    startTime: {
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW
    },
    endTime: {
      type: DataTypes.DATE
    }
  }, {
    classMethods: {
      associate: function (models) {
        ChannelMetadata.belongsTo(models.Channel);
      }
    },
    timestamps: false
  });
  
  return ChannelMetadata;
}