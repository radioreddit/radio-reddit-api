var Promise = require('bluebird');

module.exports = function (sequelize, DataTypes) {
  var User = sequelize.define('User', {
    username: { // Reddit Username
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        is: /^[\w-]+$/, // https://github.com/reddit/reddit/blob/master/r2/r2/lib/validator/validator.py#L1350
        len: [3, 20], // https://github.com/reddit/reddit/blob/master/r2/r2/lib/validator/validator.py#L1347
        notEmpty: true
      },
      unique: true
    },
    firstName: {
      type: DataTypes.STRING
    },
    lastName: {
      type: DataTypes.STRING
    },
    email: {
      type: DataTypes.STRING,
      validate: {
        isEmail: true
      },
      unique: true
    },
    lastActive: {
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW
    },
    legacyId: { // Old ID for users imported from the old Drupal site
      type: DataTypes.INTEGER
    }
  }, {
    classMethods: {
      associate: function (models) {
        User.belongsToMany(models.Artist, {through: 'UsersArtists'});
        User.belongsToMany(models.Track, {as: 'Favorite', through: 'UsersFavorites'});
        User.hasMany(models.Vote);
        User.hasMany(models.Track);
        User.hasMany(models.Playlist);
      },
      
      getUserWritableProperties: function () {
        return ['firstName', 'lastName', 'email'];
      }
    },
    
    instanceMethods: {
      isWritableByUser: function (user) {
        var self = this;
        
        return new Promise(function (resolve, reject) {
          resolve(self.username === user.username);
        });
      }
    },
  
    paranoid: true
  });
  
  return User;
}