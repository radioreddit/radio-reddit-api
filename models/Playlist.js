module.exports = function (sequelize, DataTypes) {
  var Playlist = sequelize.define('Playlist', {
    title: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    isPublic: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    }
  }, {
    classMethods: {
      associate: function (models) {
        Playlist.belongsTo(models.User);
        Playlist.belongsToMany(models.Track, {through: models.PlaylistsTracks});
      }
    },
    instanceMethods: {
      isWritableByUser: function (user) {
        return user.hasPlaylist(this);
      }
    },
    paranoid: true
  });
  
  return Playlist;
}