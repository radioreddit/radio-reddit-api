module.exports = function (sequelize, DataTypes) {
  var Vote = sequelize.define('Vote', {
    isUpVote: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    TrackId: {
      type: DataTypes.INTEGER,
      unique: 'trackUserConstraint'
    },
    UserId: {
      type: DataTypes.INTEGER,
      unique: 'trackUserConstraint'
    }
  }, {
    classMethods: {
      associate: function (models) {
        Vote.belongsTo(models.Track);
        Vote.belongsTo(models.User);
      }
    },
    paranoid: true
  });
  
  return Vote;
}