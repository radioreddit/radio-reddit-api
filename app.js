require('newrelic');
bugsnag = require('bugsnag');

var AWS = require('aws-sdk'); 
var s3 = new AWS.S3(); 

var bodyParser = require('body-parser');
var errors = require(__dirname + '/lib/errors.js');
var express = require('express');
var packageInfo = require(__dirname + '/package.json');
var rest = require('epilogue');
var request = require('request-promise');
var session = require('express-session');
var RedisStore = require('connect-redis')(session);
var s3Util = require(__dirname + '/lib/s3Util.js');
var Sequelize = require('sequelize');
var Snoocore = require('snoocore');
var winston = require('winston');
var _ = require('underscore');
require('winston-logstash');

// Set up app log
global.applog = new (winston.Logger)({
  transports: [
    new (winston.transports.Console)({
      colorize: true,
      timestamp: true,
      level: 'debug',
      handleExceptions: true
    })
  ]
});

applog.info(packageInfo.description + ' starting up');

// Config
applog.debug('Loading config');
var config = require(__dirname + '/conf/config.json');
config = config || {};
config.server = config.server || {};
config.server.port = config.server.port || process.env.PORT || 80;
config.server.bindAddress = config.server.bindAddress || '0.0.0.0';

// Bugsnag
if (config.bugsnag && config.bugsnag.key) {
  bugsnag.register(config.bugsnag.key);
  process.on('unhandledRejection', function (err, promise) {
    applog.error('Unhandled rejection: ' + (err && err.stack || err));
    bugsnag.notify(err);
  });
}

// Extra Logging (Logstash, etc.)
if (config.logging && config.logging.transports) {
  _.each(config.logging.transports, function (transport) {
    applog.info('Adding logger', transport.type);
    applog.add(winston.transports[transport.type], transport.config);
  });
}

// Database
var sequelize = new Sequelize(config.db.name, config.db.username, config.db.password, {
  logging: applog.debug,
  dialect: 'postgres',
  host: config.db.host,
  port: config.db.port
});

sequelize
  .authenticate()
  .then(function () {
    applog.debug('Connected to database');
  }).catch(function (err) {
    applog.error('Error connecting to database', err.message);
  });

var models = require(__dirname + '/models/index.js')(sequelize);
sequelize.sync();

// Reddit API
var reddit = new Snoocore({
  userAgent: ['web', packageInfo.name, packageInfo.version].join(':') + ' (by /u/bradisbell)', // https://github.com/reddit/reddit/wiki/API
  oauth: {
    type: 'explicit',
    duration: 'permanent',
    mobile: true,
    key: config.redditApi.key,
    secret: config.redditApi.secret,
    redirectUri: config.redditApi.oauthRedirectUri,
    scope: ['identity', 'submit', 'vote', 'read','modposts']
  }
});

// API server
var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(
  session(
    _.extend(
      {
        saveUninitialized: false,
        resave: true
      },
      _.omit(config.session, 'redisStore'),
      {
        store: new RedisStore(config.session.redisStore)
      }
    )
  )
);

app.use(function (req, res, next) {
  if (!req.session) {
    return next(new errors.InternalServerError('Session data failure'));
  }
  next();
});

app.use(function (req, res, next) {
  applog.info('HTTP Request', {method: req.method, url: req.url});
  var origin = req.get('origin');
  if(origin && origin.match('^http(s)?://.*'+config.cors.domain.replace(/[.*+?^${}()|[\]\\]/g, "\\$&")+'$')){
    res.header('Access-Control-Allow-Origin', origin);
    res.header('Access-Control-Allow-Credentials', 'true');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
  }
  res.header('Vary', 'Origin');
  return next();
});

app.use('/auth', require(__dirname + '/routes/auth.js')(reddit, models));
app.use('/artists', require(__dirname + '/routes/artists.js')(sequelize, models));

app.use(require(__dirname + '/routes/channels.js')(sequelize, models, config.metadataUpdateAuth));
app.use('/votes', require(__dirname + '/routes/votes.js')(sequelize, models, reddit));
app.use(require(__dirname + '/routes/tracks.js')(sequelize, models, reddit));

_.each(['albums', 'users', 'playlists'], function (routeName) {
  app.use(require(__dirname + '/routes/' + routeName + '.js')(sequelize, models));
});

rest.initialize({
  app: app,
  sequelize: sequelize
});

rest.resource({
  model: models.Feed,
  endpoints: ['/feeds', '/feeds/:id']
});

rest.resource({
  model: models.Genre,
  endpoints: ['/genres', '/genres/:id'],
  actions: ['list', 'read']
});

// Self-documenting... a bit weird but ensures the docs are always up-to-date with whatever API server is being hit
app.use('/doc', express.static(__dirname + '/doc'));

// Error Handling Middleware
app.use(function (err, req, res, next) {
  applog.error(err.stack);
  
  if (!err.code) {
    err = new errors.InternalServerError();
  }

  res
    .status(err.code)
    .send({
      error: err.message
    });
});


applog.info('Listening on ' + config.server.bindAddress + ':' + config.server.port);
app.listen(config.server.port, config.server.bindAddress, function () {
  if ((config.gid || config.uid) && !(process.setgid || process.setuid)) {
    applog.warn('GID and UID are configured but unavailable on the current system.');
    return;
  }
  
  if (config.gid) {
    applog.info('Setting process GID', config.gid);
    process.setgid(config.gid);
  }
  if (config.uid) {
    applog.info('Setting process UID', config.uid);
    process.setuid(config.uid);
  }
});

// Internal cron job for checking transcode status via polling
function checkTranscodeStatus () {
  models.Track.findAll({
    where: {
      state: 'Uploaded'
    },
    limit: 5
  }).then(function (tracks) {
    return Promise.all(_.map(tracks, function (track) {
      applog.debug('Testing track media', track.id);
      return Promise.all(_.map(track.media, function (mediaUrl) {
        applog.debug('Testing media URL', mediaUrl);
        return request(mediaUrl).promise(); // Can't do a HEAD request as S3 doesn't support it in a pre-signed URL
      })).then(function () {
        applog.debug('Setting track to \'Approved\'', track.id);
        track.state = 'Approved';
        return track.save();
      }).catch(function (e) {
        applog.debug('Track media test failed', track.id, e);
        if (track.updatedAt < (new Date(new Date() - 15*60*1000))) {
          applog.debug('Transcode timeout elapsed.');
          track.state = 'Rejected';
          track.save();
        }
      });
    }));
  }).then(function () {
    setTimeout(checkTranscodeStatus, 20000);
  })
}
checkTranscodeStatus();