function defineError (name, originalMessage, code) {
  var newErrorType = function (message) {
    Object.defineProperty(this, 'message', {
      value: message || originalMessage || '',
      enumerable: false
    });
    
    Object.defineProperty(this, 'code', {
      value: code || 500,
      enumerable: false
    });

    Error.captureStackTrace(this, newErrorType);
  }
  newErrorType.prototype = new Error();
  newErrorType.prototype.name = name;
  
  return newErrorType;
}

module.exports = {
  UnauthorizedError: defineError('UnauthorizedError', 'Unauthorized', 401),
  ForbiddenError: defineError('ForbiddenError', 'Access to this resource is forbidden', 403),
  NotFoundError: defineError('NotFoundError', 'The requested resource was not found', 404),
  InternalServerError: defineError('InternalServerError', 'An unspecified error has occurred', 500)
}