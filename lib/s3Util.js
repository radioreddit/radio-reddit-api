var AWS = require('aws-sdk');
var config = require(__dirname + '/../conf/config.json');
var crypto = require('crypto');
var _  = require('underscore');

var s3 = new AWS.S3();

// https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_Objects/encodeURIComponent%23Examples
function encodeRFC5987ValueChars (str) {
  return encodeURIComponent(str).
    // Note that although RFC3986 reserves "!", RFC5987 does not,
    // so we do not need to escape it
    replace(/['()]/g, escape). // i.e., %27 %28 %29
    replace(/\*/g, '%2A').
    
    // The following are not required for percent-encoding per RFC5987, 
    // so we can allow for a little better readability over the wire: |`^
    replace(/%(?:7C|60|5E)/g, unescape);
}

function clone(a) {
  //Simple way for us to do a deep clone
  return JSON.parse(JSON.stringify(a));
}

function getUploadFormInfo (keyVal) {
    var formFields = {};
    var policy = {
      conditions: [
        {bucket: config.s3.bucket}, 
        ['content-length-range', 1, config.s3.maxUploadSize],
        ['starts-with', '$key', keyVal],
        {acl: 'private'}
      ]
    };
    
    var expiration = new Date(); 
    expiration.setHours(expiration.getHours()+2);
    policy.expiration =  expiration.toISOString();

    var policyBase64 = Buffer(
      JSON.stringify(policy)
    ).toString('base64');

    var signature = crypto
      .createHmac('sha1', s3.config.credentials.secretAccessKey)
      .update(new Buffer(policyBase64, 'utf-8'))
      .digest('base64');

    formFields.AWSAccessKeyId = s3.config.credentials.accessKeyId;
    formFields.acl = 'private';
    formFields.key = keyVal;
    formFields.policy = policyBase64;
    formFields.signature = signature
    formFields.bucket = config.s3.bucket;

    return {
      formFields: formFields,
      url: s3.endpoint.href + config.s3.bucket
    };
  }

module.exports = {
  getTrackKey: function getTrackKey (id, format) {
    return [
      'tracks',
      encodeURIComponent(id),
      encodeURIComponent(format)
    ].join('/');
  },
  
  getTrackMedia: function getTrackMedia (track) {
    var self = this;    
    var media = {};

    _.each(config.s3.trackMediaTypes, function (mediaType, mediaTypeKey) {
      var s3Options = {
        Bucket: config.s3.bucket,
        Key: self.getTrackKey(track.id, mediaTypeKey),
        Expires: 86400 
      };

      if (!mediaType.noDispositionOverride && track.Artist) {
        s3Options.ResponseContentDisposition = 
          'attachment; filename*=UTF-8\'\'' + 
          encodeRFC5987ValueChars(
            track.Artist.name + ' - ' + track.title + 
            ' (radioreddit.com).' + mediaType.extension
          );
      }

      media[mediaTypeKey] = s3.getSignedUrl('getObject', s3Options);
    });

    return media;
  },

  getTrackUploadUrl: function getTrackUploadUrl (track) {
    return s3.getSignedUrl('putObject', {
      Bucket: config.s3.bucket,
      Key: this.getTrackKey(track.id, 'original'),
      Expires: 86400
    });
    
  },
  
  getArtUploadFormInfo: function getTrackUploadFormInfo(keyVal) {
    var uploadFormInfo = getUploadFormInfo(['art','album',keyVal].join('/')); 
    return uploadFormInfo;
  },
  
  getTrackUploadFormInfo: function getTrackUploadFormInfo(keyVal) {
    var uploadFormInfo = getUploadFormInfo(['tracks',keyVal,'original'].join('/'));
    return uploadFormInfo;    
  },
  
  getAlbumKey: function getAlbumKey (id,coverImage){
    return [
      'art',
      'album',
      encodeURIComponent(id),
      encodeURIComponent(coverImage)
    ].join('/');
  },
  
  getAlbumArt: function getAlbumArt (album){
    if(!album.coverImage){
      return;
    }
    var self = this;    
    var s3Options = {
      Bucket: config.s3.bucket,
      Key: self.getAlbumKey(album.id, album.coverImage),
      Expires: 86400 
    };
    return s3.getSignedUrl('getObject', s3Options);
  }
};