var errors = require(__dirname + '/errors.js');

module.exports = function (models) {
  return {
    auth: function auth (req, res, next) {
      if (!req.session.userId) {
        next(new errors.UnauthorizedError());
      }
      
      models.User.findById(req.session.userId).then(function (user) {
        req.user = user;
        next();
        
      }).catch(next);
    },
    
    artistAuth: function artistAuth (req, res, next) {
      models.Artist.findById(req.params.artistId).then(function (artist) {
        if (!artist) {
          throw new errors.NotFoundError();
        }
        
        return artist.isWritableByUser(req.user).then(function (writable) {
          if (!writable) {
            throw new errors.UnauthorizedError();
          }
          req.artist = artist;
          
          next();
        });
        
      }).catch(next);
    },
    
    albumAuth: function albumAuth (req, res, next) {
      models.Album.findById(req.params.albumId).then(function (album) {
        if (!album) {
          throw new errors.NotFoundError();
        }
        
        return album.isWritableByUser(req.user).then(function (writable) {
          if (!writable) {
            throw new errors.UnauthorizedError();
          }
          req.album = album;
          
          next();
        });
        
      }).catch(next);
    },
    
    trackAuth: function trackAuth (req, res, next) {
      models.Track.findById(req.params.trackId).then(function (track) {
        if (!track) {
          throw new errors.NotFoundError();
        }
        
        return track.isWritableByUser(req.user).then(function (writable) {
          if (!writable) {
            throw new errors.UnauthorizedError();
          }
          req.track = track;
          
          next();
        });
        
      }).catch(next);
      
    },
    
    userAuth: function userAuth (req, res, next) {
      models.User.findById(req.params.userId).then(function (user) {
        if (!user) {
          throw new errors.NotFoundError();
        }
        
        return user.isWritableByUser(req.user).then(function (writable) {
          if (!writable) {
            throw new errors.UnauthorizedError();
          }
          
          req.writableUser = user;
          next();
        });
        
      }).catch(next);
      
    },

    playlistAuth: function playlistAuth (req, res, next) {
      models.Playlist.findById(req.params.playlistId).then(function (playlist) {
        if (!playlist) {
          throw new errors.NotFoundError();
        }

        return playlist.isWritableByUser(req.user).then(function (writable) {
          if (!writable) {
            throw new errors.UnauthorizedError();
          }
          
          req.playlist = playlist;
          next();
        });
        
      });
    }
  };

} 

