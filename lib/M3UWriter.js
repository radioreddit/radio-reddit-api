var stream = require('stream');
module.exports = function () {
  var playlistBuilder = new stream.Transform({objectMode: true});
  playlistBuilder.push('#EXTM3U\n\n');
  playlistBuilder._transform = function (item, encoding, done) {
    this.push(item + '\n');
    done();
  }
  
  playlistBuilder._flush = function (done) {
    done();
  }  

  return playlistBuilder;

}
