#!/usr/bin/env bash

apt-get update

echo "Installing Tools"
apt-get install -y htop tcpdump

echo "Installing PostgreSQL"
apt-get install -y postgresql postgresql-contrib libpq-dev build-essential
echo "host all all 192.168.33.17/16 trust" >> /etc/postgresql/9.3/main/pg_hba.conf 
echo "listen_addresses = '*'" >> /etc/postgresql/9.3/main/postgresql.conf
service postgresql restart
sudo -u postgres psql -U postgres -d postgres -c "ALTER USER postgres WITH PASSWORD 'password';"
su - postgres -c "createdb radioreddit"

echo "Installing Redis"
apt-get install -y redis-server

echo "Installing Node.js"
curl -s https://nodejs.org/dist/v4.4.4/node-v4.4.4-linux-x64.tar.gz | tar xzf - --strip-components=1 --directory /usr/local

echo "Installing NPM modules"
cd /vagrant
sudo -u vagrant HOME=/tmp npm --no-color --no-bin-links install
