var epilogue = require('epilogue');
var express = require('express');
var _ = require('underscore');

module.exports = function users (sequelize, models) {
  var middleware = require(__dirname + '/../lib/middleware.js')(models);
  var router = express.Router();

  router.get('/users/current', function (req, res, next) {
    if (!req.session.userId) {
      return res.sendStatus(404);
    }

    models.User.findOne({
      where: {
        id: req.session.userId
      },
      include:[{
        model: models.Artist
      }]
    }).then(function (user) {
      res.send(user);

    }).catch(function (err) {
      applog.error(err);
      next(err);

    });
  });

  router.get(
    '/users/:userId',
    middleware.auth,
    middleware.userAuth,
    function (req, res, next) {
      res.send(req.writableUser);
    }
  );
  
  router.get(
    '/users/:userId/artists',
    middleware.auth,
    middleware.userAuth,
    function (req, res, next) {
      models.Artist.findAll({
        include: [{
          model: models.User,
          where: {
            id: req.user.id
          },
          attributes: []
        }]        
      }).then(function (results){
        res.send(results);
      }).catch(function (err) {
        next(err);
      });
    }
  );

  router.put('/users/:userId', middleware.auth, function (req, res, next) {
    req.user.updateAttributes(
      _.pick(req.body, models.User.getUserWritableProperties())
    ).then(function () {
      res.send(req.user);
    }).catch(next);

  });
  
  return router;
}
