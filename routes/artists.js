var epilogue = require('epilogue');
var express = require('express');

module.exports = function artists (sequelize, models) {
  var middleware = require(__dirname + '/../lib/middleware.js')(models);
  var router = express.Router();


  router.post('/', middleware.auth, function (req, res, next) {
    
    models.Artist.create(req.body).then(function (artist) {
      return artist.addUser(req.user).then(function () {
        res.send(artist);
      });
    }).catch(next);
    
  });


  router.put(
    '/:artistId',
    middleware.auth, 
    middleware.artistAuth,
    function (req, res, next) {

      req.artist.updateAttributes(req.body).then(function () {
        res.send(req.artist);
      }).catch(next);

    }
  );
  
  epilogue.initialize({
    app: router,
    sequelize: sequelize
  });
  
  epilogue.resource({
    model: models.Artist,
    endpoints: ['/', '/:id'],
    actions: ['list', 'read'],
    include: [
      models.Genre
    ]
  });

  return router;
}