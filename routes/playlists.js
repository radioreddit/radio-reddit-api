var epilogue = require('epilogue');
var errors = require(__dirname + '/../lib/errors.js');
var express = require('express');
var Promise = require('bluebird');

module.exports = function plyalists (sequelize, models) {
  var middleware = require(__dirname + '/../lib/middleware.js')(models);
  var router = express.Router();
  
  router.post(
    '/playlists',
    middleware.auth,
    function (req, res, next) {
      return models.Playlist.create(req.body).then(function (playlist) {
        return playlist.setUser(req.user).then(function () {
          res.send(playlist);
        });
      }).catch(next);
    }
  );
  
  router.put(
    '/playlists/:playlistId',
    middleware.auth,
    middleware.playlistAuth,
    function (req, res, next) {
      
      req.playlist.updateAttributes(req.body).then(function () {
        res.send(req.playlist);
      }).catch(next);
      
    }
  );
  
  router.put(
    '/playlists/:playlistId/tracks/:trackId',
    middleware.auth,
    middleware.playlistAuth,
    function (req, res, next) {
      models.Track.findById(req.params.trackId).then(function (track) {
        if (!track) {
          throw new errors.NotFoundError();
        }

        return req.playlist.addTrack(track).then(function () {
          res.status(205).end();
        });

      }).catch(next);

    }
  );
  
  router.delete(
    '/playlists/:playlistId/tracks/:trackId',
    middleware.auth,
    middleware.playlistAuth,
    function (req, res, next) {
      models.Track.findById(req.params.trackId).then(function (track) {
        if (!track) {
          throw new errors.NotFoundError();
        }

        return req.playlist.removeTrack(track).then(function () {
          res.status(205).end();
        });

      }).catch(next);

    }

  )
  
  epilogue.initialize({
    app: router,
    sequelize: sequelize
  });
  
  epilogue.resource({
    model: models.Playlist,
    endpoints: ['/playlists', '/playlists/:id'],
    actions: ['list', 'read'],
    include: [
      {
        model: models.Track,
        include: [
          {
            model: models.Artist,
            attributes: ['id','name']
          },{
            model: models.Album,
            attributes: ['id','title']
          }
        ]
      }
    ]
  });
  
  return router;
}