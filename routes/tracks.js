var epilogue = require('epilogue');
var errors = require(__dirname + '/../lib/errors.js');
var express = require('express');
var Promise = require('bluebird');
var config = require(__dirname + '/../conf/config.json');
var s3Util = require(__dirname + '/../lib/s3Util.js');

module.exports = function tracks (sequelize, models, reddit) {
  var middleware = require(__dirname + '/../lib/middleware.js')(models);
  var router = express.Router();

  router.post(
    '/albums/:albumId/tracks/', 
    middleware.auth,
    middleware.albumAuth,
    function (req, res, next) {
      
      req.album.getArtist().then(function (artist) {
        var newTrack = req.body;
        newTrack.state = 'Created';
        return models.Track.create(req.body).then(function (track) {
          return Promise.all([
            track.setUser(req.user),
            track.setArtist(artist),
            track.setAlbum(req.album)
          ]).then(function () {
            res.send(track);
          });
        });
        
      }).catch(next);

    }
  );
  
  router.put(
    '/tracks/:trackId',
    middleware.auth,
    middleware.trackAuth,
    function (req, res, next) {
      
      req.track.updateAttributes(req.body).then(function () {
        res.send(req.track);
      }).catch(next);
      
    }
  );
  
  router.delete(
    '/tracks/:trackId',
    middleware.auth,
    middleware.trackAuth,
    function (req, res, next) {
      req.track.destroy().then(function () {
        return res.sendStatus(204);
      }).catch(next);
    }
  );

  router.get('/tracks', function (req, res, next) {
    var limit = (req.query.limit) ? req.query.limit : 100;
    var offset = (req.query.page) ? req.query.page * limit : 0;
    if (!req.query.title && !req.query.artist && !req.query.album && !req.query.any) {
      models.Track.findAll({
        include: [
          {
            model: models.Artist,
            attributes: ['id','name'],
          },{
            model: models.Album,
            attributes: ['id','title'],
          }
        ],
        where: {
          state: {
            in: ['Approved']
          }
        },
        order: [
          ['updatedAt','DESC']
        ],
        limit: limit,
        offset: offset
      }).then(function (tracks){
        res.send(tracks);
      });
    } else {
      
      var trackTitle = (req.query.title) ? ['"Track".state = \'Approved\' AND "Track".title ilike ?',req.query.title+'%'] : [''];
      var artistName = (req.query.artist) ? ['"Track".state = \'Approved\' AND "Artist".name ilike ?',req.query.artist+'%'] : [''];
      var albumTitle = (req.query.album) ? ['"Track".state = \'Approved\' AND "Album".title ilike ?',req.query.album+'%'] : [''];
      if(req.query.any){
        trackTitle = ['"Track".state = \'Approved\' AND ("Track".title ilike ? OR "Artist".name ilike ? OR "Album".title ilike ?)',req.query.any+'%',req.query.any+'%',req.query.any+'%'];
      }
      models.Track.findAll({
        include: [
          {
            model: models.Artist,
            attributes: ['id','name'],
            where: artistName
          },{
            model: models.Album,
            attributes: ['id','title'],
            where: albumTitle
          }
        ],
        where: trackTitle,
        order: [
          [models.Artist, 'name', 'ASC'],
          [models.Album, 'createdAt', 'DESC'],
          ['albumSequence', 'ASC'],
          ['updatedAt', 'DESC']
        ],
        limit: limit,
        offset: offset
      }).then(function (tracks) {
        res.send(tracks);
        
      }).catch(function (err) {
        return next(err);

      });
    }
  });
  
  router.get('/tracks/unapproved', function (req, res, next) {
    models.Track.findAll({
      include: [
        {
          model: models.Artist,
          attributes: ['id','name'],
        },{
          model: models.Album,
          attributes: ['id','title'],
        }
      ],
      where: {
        state: {
          in: ['Created','Uploaded']
        }
      }
    }).then(function (tracks) {
      res.send(tracks);
    }).catch(next);  
  });
  
  router.get(
    '/tracks/:trackId/uploadUrl', 
    middleware.auth,
    middleware.trackAuth,
    function (req, res, next) {
      models.Track.findById(req.params.trackId).then(function (track) {
        if (!track) {
          throw new errors.NotFoundError();
        }
        
        return res.send({
          url: s3Util.getTrackUploadUrl(track)
        });
        
      }).catch(next);
    }
  );

  router.get(
    '/tracks/:trackId/uploadFormData', 
    middleware.auth,
    middleware.trackAuth,
    function (req, res, next) {
      models.Track.findById(req.params.trackId).then(function (track) {
        if (!track) {
          throw new errors.NotFoundError();
        }
        
        res.send(
          s3Util.getTrackUploadFormInfo(track.id)
        );
        
      }).catch(next);
    }
  );
  
  router.get('/tracks/:id/media/:type', function (req, res, next) {
    models.Track.findById(req.params.id).then(function (track) {
      res.redirect(track.media[req.params.type]);
    }).catch(next);
  });
  
  router.get('/tracks/posts', function(req, res, next){
    var query = req.query.q
    reddit('/r/' + config.redditApi.subreddit + '/search').get({
      q: query,
      restrict_sr: true,
      limit: 5
    }).then(function(slice) {
      res.send(slice);
    }).catch(next);
  });
  
  router.get('/tracks/hot', function(req, res, next){
    reddit('/r/' + config.redditApi.subreddit + '/hot').get({
      limit: 50
    }).then(function(slice) {
      var redditJson = slice.data;
      var redditPostFullnames = [];
      redditJson.children.forEach(function (value, index){
        if(!value.data.sticked){
          redditPostFullnames.push(value.data.name);
        }
      });
      return models.Track.findAll({
        include: [
          {
            model: models.Artist,
            attributes: ['id','name'],
          },{
            model: models.Album,
            attributes: ['id','title'],
          }
        ],
        where: {
          state: {
            in: ['Approved']
          },
          redditPostFullname: {
            in: redditPostFullnames
          }
        },
        order: [
          ['updatedAt','DESC']
        ]
      }).then(function (tracks){
        res.send(tracks);
      });
    }).catch(next);
  });
  
  router.post('/tracks/posts', middleware.auth, function (req, res, next) {
    models.Track.findById(req.body.trackId).then(function (track) {
      return reddit('/api/submit').post({
        sr: config.redditApi.subreddit,
        kind: 'link',
        url: req.body.url,
        title: req.body.title,
        captcha: req.body.captcha,
        iden: req.body.iden,
        trackId: req.body.trackId,
        api_type: 'json'
      }).then(function (result) {
        var fullname = result.json.data.name;
        track.updateAttributes({
          redditPostFullname: fullname
        });
        res.send(result);
      }).catch(function (err) {
        applog.info(err);
        track.destroy();
        throw err;
      });
    }).catch(next);
  });
  
  router.post('/tracks/approve',  middleware.auth, function (req, res, next) {
    models.Track.findById(req.body.trackId).then(function (track) {
      return reddit('/api/approve').post({
        id: track.redditPostFullname
      }).then(function (result) {
        track.updateAttributes({
          state: 'Approved'
        });
        res.send(result);
      });
    });
  });
  
  router.post('/tracks/deny',  middleware.auth, function(req, res, next){
    models.Track.findById(req.body.trackId).then(function (track) {
      return reddit('/api/remove').post({
        id: track.redditPostFullname
      }).then(function(result){
        track.updateAttributes({
          state: 'Rejected'
        });
        res.send(track);
      });
    });
  });

  router.get('/tracks/:trackId/post', function(req, res, next){
    var trackId = req.params.trackId;
    models.Track.findOne({
      where: {
        id: trackId
      }
    }).then(function(track){
      if (!track.redditPostFullname) {
        return res.sendStatus(404);
      }
      return reddit('/by_id/' + encodeURIComponent(track.redditPostFullname)).get().then(function (listing) {
        res.send(listing.data.children[0].data);
      });
    }).catch(next);
  });

  epilogue.initialize({
    app: router,
    sequelize: sequelize
  });

  epilogue.resource({
    model: models.Track,
    endpoints: ['/tracks', '/tracks/:id'],
    actions: ['read'],
    include: [
      {
        model: models.Artist,
        attributes: ['id','name']
      },{
        model: models.Album,
        attributes: ['id','title']
      }
    ]
  });
  
  return router;
}