var express = require('express');

module.exports = function votes (sequelize, models, reddit){
  var middleware = require(__dirname + '/../lib/middleware.js')(models);
  var router = express.Router();
  router.get('/users/:userId', middleware.auth, function(req, res, next){
    models.Vote.findAll({
      where:{
        UserId: req.params.userId
      }
    }).then(function(votes){
      res.send(votes);
    }).catch(next);
  });
  router.get('/users/:userId/tracks/:tracks', middleware.auth, function(req, res, next){
    models.Vote.findAll({
      where:{
        TrackId: {
          in: req.params.tracks.split(',')
        },
        UserId: req.params.userId
      }
    }).then(function(votes){
      res.send(votes);
    }).catch(next);
  });
  
  router.post('/tracks/:trackId', middleware.auth, function(req, res, next){
    var isUpVote = req.body.isUpVote;
    var trackId = req.params.trackId;
    models.Track.findOne({
      where: {
        id: trackId
      }
    }).then(function (track) {
      if (!track) {
        throw new Error('Track not found');
      }
      if (!track.redditPostFullname) {
        throw new Error('Track does not have a Reddit post to vote on.');
      }
      
      // TODO determine what to do if we dont have a fullname returned. Perform a search on the subreddit?
      return reddit('/by_id/'+encodeURIComponent(track.redditPostFullname)).get().then(function(listing){
        return reddit('/api/vote').post({
          dir: (isUpVote) ? 1 : -1,
          id: track.redditPostFullname,
          uh: listing.data.modhash
        }).then(function(slice) {
          return models.Vote.upsert({
            TrackId: trackId,
            isUpVote: isUpVote,
            UserId: req.session.userId
          }).then(function(){
            res.send(slice);
          });
        });
      });
      
    }).catch(next);
    
  });
  
  return router;
}