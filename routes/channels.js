var basicAuth = require('basic-auth');
var epilogue = require('epilogue');
var errors = require(__dirname + '/../lib/errors.js');
var express = require('express');
var M3UWriter = require(__dirname + '/../lib/M3UWriter.js');
var _ = require('underscore');

module.exports = function channels (sequelize, models, metadataUpdateAuth) {
  var router = express.Router();

  var auth = function (req, res, next) {
    function unauthorized(res) {
      res.set('WWW-Authenticate', 'Basic realm=Authorization Required');
      return res.send(401);
    };

    var user = basicAuth(req);

    if (!user || !user.name || !user.pass) {
      return unauthorized(res);
    };

    if (user.name === metadataUpdateAuth.username && user.pass === metadataUpdateAuth.password) {
      return next();
    } else {
      return unauthorized(res);
    };
  };

  epilogue.initialize({
    app: router,
    sequelize: sequelize
  });

  epilogue.resource({
    model: models.Channel,
    endpoints: ['/channels', '/channels/:id'],
    actions: ['list', 'read'],
    include: [
      models.Stream
    ]
  });
  
  router.get('/channels/:id/metadata', function (req, res, next) {
    models.ChannelMetadata.findAll({
      where: {
        ChannelId: req.params.id
      },
      order: [
        ['startTime', 'DESC']
      ],
      limit: 10
    }).then(function (metadata) {
      res.send(metadata);
    }).catch(next);
  });

  router.post('/channels/:id/metadata', auth, function (req, res, next) {
    models.Channel.findById(req.params.id).then(function (channel) {
      return models.ChannelMetadata.create(req.body).then(function (newMetadata) {
        return models.ChannelMetadata.update(
          {
            endTime: new Date()
          },
          {
            where: {
              ChannelId: channel.id
            }
          }
        ).then(function () {
          return channel.addChannelMetadata(newMetadata).then(function () {
            return newMetadata;
          });
        })

      });
    }).then(function (newMetadata) {
      res.send(newMetadata);

    }).catch(next);

  });
  

  router.get('/channels/:id/sourceList/:format', function (req, res, next){
    
    var playlistFormats = {
      m3u: {
        writer: M3UWriter,
        contentType: 'audio/mpegurl'
      }
    }
    var writer = new playlistFormats[req.params.format].writer();
    
    sequelize.query(' \
      SELECT "TracksGenres"."TrackId" AS "id" \
      FROM "ChannelGenres", "TracksGenres" \
      WHERE \
        "ChannelGenres"."ChannelId" = ? AND \
        "TracksGenres"."GenreId" = "ChannelGenres"."GenreId"; \
    ',
    {
      model: models.Track,
      raw: false,
      replacements: [req.params.id]
    }).then(function (tracks) {
      res.header('Content-Type', playlistFormats[req.params.format].contentType);
      writer.pipe(res);
      
      _.each(_.sample(tracks, 1000), function (track) {
        writer.write('http://' + req.headers.host + '/tracks/' + track.id + '/media/original?.mp3'); // This is dumb because Liquidsoap is extremely buggy and can't handle URLs properly.
      });
      
      writer.end();
      
    }).catch(next);
      
  });


  return router;
}
