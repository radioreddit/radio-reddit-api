var epilogue = require('epilogue');
var errors = require(__dirname + '/../lib/errors.js');
var express = require('express');
var s3Util = require(__dirname + '/../lib/s3Util.js');
var _ = require('underscore');

module.exports = function albums (sequelize, models) {
  var middleware = require(__dirname + '/../lib/middleware.js')(models);
  var router = express.Router();

  router.post(
    '/artists/:artistId/albums/', 
    middleware.auth, 
    middleware.artistAuth,
    function (req, res, next) {
      
      models.Album.create(req.body).then(function (album) {
        return album.setArtist(req.artist).then(function () {
          res.send(album);
        });
      }).catch(next);
      
    }
  );
  
  router.put(
    '/albums/:albumId',
    middleware.auth,
    middleware.albumAuth,
    function (req, res, next) {
      
      req.album.updateAttributes(req.body).then(function () {
        res.send(req.album);
      }).catch(next);
      
    }
  );
  
  router.delete(
    '/albums/:albumId',
    middleware.auth,
    middleware.albumAuth,
    function (req, res, next) {
      req.album.getTracks().then(function (tracks) {
        return Promise.all(_.map(tracks, function (track) {
          return track.destroy();
        }));
      }).then(function () {
        return req.album.destroy();
      }).then(function () {
        return res.sendStatus(204);
      }).catch(next);
    }
  );
  
  router.get(
    '/albums/:albumId/artUploadFormData',
    middleware.auth,
    middleware.albumAuth,
    function (req, res, next) {
      models.Album.findById(req.params.albumId).then(function (album) {
        if (!album) {
          throw new errors.NotFoundError();
        }
        
        var uploadFormInfo = s3Util.getArtUploadFormInfo(album.id);
        res.send(uploadFormInfo);
      }).catch(next);
    }
  );
  
  epilogue.initialize({
    app: router,
    sequelize: sequelize
  });
  
  epilogue.resource({
    model: models.Album,
    endpoints: ['/albums', '/albums/:id'],
    actions: ['list', 'read'],
    include: [ models.Track, models.Artist ]
  });

  return router;
}