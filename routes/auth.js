var crypto = require('crypto');
var errors = require(__dirname + '/../lib/errors.js');
var express = require('express');
var url = require('url');

module.exports = function auth (reddit, models) {
  var router = express.Router();
  
  router.post('/logout', function (req, res, next) {
    req.session.destroy();
    res.status(205).end();
  });

  router.get('/', function (req, res, next) {
    if (req.query.error) {
      if(req.session.postAuthRedirectUrl){       
        var postRedirectUrl = url.parse(req.session.postAuthRedirectUrl, true, false);
        postRedirectUrl.query['error'] = req.query.error;
        delete req.session.postAuthRedirectUrl;
        return res.redirect(url.format(postRedirectUrl));
      }
      throw new errors.InternalServerError(req.query.error);
    }
    
    if (req.query.code && req.query.state) {
      if (req.session.csrfState !== req.query.state) {
        throw new errors.InternalServerError('CSRF state mismatch');
      }
      
      reddit.auth(req.query.code).then(function (refreshToken) {
        return reddit('/api/v1/me').get();
        
      }).then(function (redditData) {
        return models.User.findOrCreate({
          where: {
            username: redditData.name
          }
        }).spread(function (user, created) {
          return user;
        });

      }).then(function (user) {
        req.session.userId = user.id;
        
        if (req.session.postAuthRedirectUrl) {
          var postAuthRedirectUrl = req.session.postAuthRedirectUrl;
          delete req.session.postAuthRedirectUrl;
          
          return res.redirect(postAuthRedirectUrl);
        }
        
        res.send(user);
        
      }).catch(function (err) {
        next(err);
        
      });
      
      return;
    }
    
    if (req.query.redirectUrl) {
      req.session.postAuthRedirectUrl = req.query.redirectUrl;
    }
    req.session.csrfState = crypto.randomBytes(64).toString('hex');
    res.redirect(reddit.getExplicitAuthUrl(req.session.csrfState));
  });
  
  router.get('/captcha', function(req, res, next){
    reddit('/api/new_captcha').post({
      api_type: 'json'
    }).then(function(result){
      var iden = result.json.data.iden;
      var imageUrl = 'https://www.reddit.com/captcha/' + encodeURIComponent(iden);
      res.send({
        iden: iden,
        imageUrl: imageUrl
      });
    }).catch(next);
  });
  
  return router;
}