var config = require(__dirname + '/conf/config.json');
var packageInfo = require(__dirname + '/package.json');

exports.config = {
  app_name: packageInfo.description,
  license_key: config.newRelic.licenseKey,
  logging: {
    level: 'info'
  }
};
