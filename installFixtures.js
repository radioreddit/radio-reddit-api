var Sequelize = require('sequelize');
var sequelizeFixtures = require('sequelize-fixtures');

var config = require(__dirname + '/conf/config.json');

// Database
var sequelize = new Sequelize(config.db.name, config.db.username, config.db.password, {
  dialect: 'postgres',
  host: config.db.host,
  port: config.db.port,
  queue: false
});

sequelize.authenticate().then(function () {
  console.log('Connected to database');
  var models = require(__dirname + '/models/index.js')(sequelize);
  sequelize.sync();

  sequelizeFixtures.loadFiles([
    'fixtures/genres.json',
    'fixtures/channels.json',
    'fixtures/channelMetadata.json',
    'fixtures/streams.json'
  ], models);

}).catch(function (err) {
  console.log('Error connecting to database', err.message);
  process.exit(1);
});

